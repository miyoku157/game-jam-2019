﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [HideInInspector]
    public SectorController sectorController;
    [HideInInspector]
    public GameController gameController;

    public Transform target;
    bool isOpened;
    public bool isAlwaysOpened = false;
    public Sprite openedDoor;
    public Sprite closedDoor;
    public AudioSource recyclotron;
    public AudioSource zone1;
    private SpriteRenderer spr;

    private void Awake()
    {
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
        sectorController = FindObjectOfType<SectorController>();
        gameController = FindObjectOfType<GameController>();
    }

    void Start()
    {
        if(isAlwaysOpened)
            Open();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject colGo = collision.gameObject;

        if(colGo.CompareTag("Player") && isOpened)
        {
            colGo.transform.position = target.position;

            // Add fade + stop player movement

            // Sector gate
            if(!isAlwaysOpened)
            {
                Close();
                sectorController.NextSector();

                gameController.nextSector();
            }
            // Gate recyclotron
            else
            {
                gameController.nextWave();
                recyclotron.Stop();
                zone1.Play();
            }
        }
    }

    public void Open()
    {
        isOpened = true;
        spr.sprite = openedDoor;
    }

    public void Close()
    {
        isOpened = false;
        spr.sprite = closedDoor;
    }
}
