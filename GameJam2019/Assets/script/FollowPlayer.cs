﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;

    private Vector3 vec;

    void Start()
    {
        vec = new Vector3();
    }
    
    void Update()
    {
        if (player != null)
        {
            vec.x = player.transform.position.x;
            vec.y = player.transform.position.y;
            vec.z = -10f;

            transform.position = vec;
        }
    }
}
