﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : Dechet
{
    // Start is called before the first frame update
    [HideInInspector]
    public int type;
    [HideInInspector]
    public float PassBonus;
    public capacite itemCapacite;
    public effet itemEffet;
    private GameController cs;
    public delegate void capacite(LivingBeing joueur);
    public delegate void effet(LivingBeing joueur);
    void Start()
    {
        cs = FindObjectOfType<GameController>();
        type = Random.Range(0, 3);
        type = 0;
        if (type == 0)
        {
            PassBonus =(float) ( 1 * Random.Range(0.7f, 1.3f) + 0.1 * cs.waveCounter);
            type = Random.Range(0, 3);
            if(type == 0)
            {
                itemCapacite = dash;
            }else if (type == 1)
            {
                itemCapacite = buff;
            }
            type = Random.Range(0, 100);
            if(type < 30)
            {
                type = Random.Range(0, 4);
                if (type == 0)
                {
                    itemCapacite = ChangeLifesteal;
                }
                else if (type == 1)
                {
                    itemCapacite = ChangeMoreDamage;
                }
                else if (type == 2)
                {

                }
                else
                {

                }
            }

            type = 0;
            //attack
        }
        else if (type == 1)
        {
            //defense
            PassBonus = (float)(10 * Random.Range(0.7f, 1.3f) + 2 * cs.waveCounter *2);
        }
        else
        {
            //PV
            PassBonus = (float)(10 * Random.Range(0.7f, 1.3f)+ 7 * cs.waveCounter);
        }

        itemCapacite = dash;
        Debug.Log(type);
    }
    void ChangeLifesteal(LivingBeing joueur)
    {
        joueur.lifeVamp = !joueur.lifeVamp;
    }
    void ChangeMoreDamage(LivingBeing joueur)
    {
        joueur.MoreDamage = !joueur.MoreDamage;
    }
    void dash(LivingBeing joueur)
    {
        if(float.IsNaN(joueur.direction.x)|| float.IsNaN(joueur.direction.y)|| float.IsNaN(joueur.direction.z))
        {
            joueur.direction = Vector3.up;
        }
        Rigidbody2D rb2D = joueur.gameObject.GetComponent<Rigidbody2D>();
        RaycastHit2D hit = Physics2D.Raycast(joueur.gameObject.transform.position, (joueur.direction).normalized,Vector3.Distance(joueur.gameObject.transform.position, joueur.gameObject.transform.position + joueur.direction * 2));
        EGTween.MoveTo(joueur.gameObject, EGTween.Hash("position", joueur.gameObject.transform.position + joueur.direction * 2, "isLocal", true, "time", 0.25f));

        if (hit.collider != null)
        {
            if (Vector3.Distance(hit.point, joueur.gameObject.transform.position) > Vector3.Distance(joueur.gameObject.transform.position + joueur.direction * 2, joueur.gameObject.transform.position))
            {
                //EGTween.MoveTo(joueur.gameObject, EGTween.Hash("position", joueur.gameObject.transform.position + joueur.direction * 2, "isLocal", true, "time", 0.25f));
            }
        else
            {
                /*Vector2 temp = hit.point;
                temp.y -= 0.1f;
                temp.x -= 0.1f;
                EGTween.MoveTo(joueur.gameObject, EGTween.Hash("position", temp, "time", 0.25f));*/
            }
        }
        else
        {
            EGTween.MoveTo(joueur.gameObject, EGTween.Hash("position", joueur.gameObject.transform.position + joueur.direction * 2, "isLocal", true, "time", 0.25f));
        }

    }
    
    void buff(LivingBeing joueur)
    {
        joueur.attack *= (int)1.5;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
