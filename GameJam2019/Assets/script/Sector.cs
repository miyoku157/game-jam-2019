﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector : MonoBehaviour
{
    [HideInInspector]
    public GameController gameController;
    [HideInInspector]
    public SectorController sectorController;

    // Generation data
    public int nbTrapMin = 1;
    public int nbTrapMax = 3;
    public int nbObstacleMin = 5;
    public int nbObstacleMax = 10;
    public int nbMonsterBasic = 12;
    public int nbItem = 30;

    private int nbMonsterElite;
    private int nbTrash;

    // Grid data
    private int width = 96;
    private int height = 81;
    
    private GridBlock[,] blockList;

    [Header("NESW")]
    public bool North;
    public bool East;
    public bool South;
    public bool West;

    [Header("Spawn pool")]
    public GameObject objects;

    [Header("Gate")]
    public GameObject gate;

    enum blockType {Wall, Trap, Obstacle};

    private void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        sectorController = FindObjectOfType<SectorController>();
    }

    void launchASTAR()
    {
        AstarPath.active.Scan();
    }

    public void Generate()
    {
        blockList = new GridBlock[width, height];
        
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < height; j++)
            {
                blockList[i, j] = new GridBlock(i, j);
            }
        }

        // Gate interdiction zones
        if(North)
        {
            for(int i = width / 2 - 2; i < width / 2 + 2; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    blockList[i, j].isEmpty = false;
                }
            }
        }

        if(South)
        {
            for(int i = width / 2 - 2; i < width / 2 + 2; i++)
            {
                for(int j = height - 2; j < height; j++)
                {
                    blockList[i, j].isEmpty = false;
                }
            }
        }

        if(West)
        {
            for(int i = width - 2; i < width; i++)
            {
                for(int j = height / 2 - 2; j < height / 2 + 2; j++)
                {
                    blockList[i, j].isEmpty = false;
                }
            }
        }

        if(East)
        {
            for(int i = 0; i < 2; i++)
            {
                for(int j = height / 2 - 2; j < height / 2 + 2; j++)
                {
                    blockList[i, j].isEmpty = false;
                }
            }
        }

        int nbTrapToSpawn = Random.Range(nbTrapMin, nbTrapMax + 1);
        int nbObstacleToSpawn = Random.Range(nbObstacleMin, nbObstacleMax + 1);
        int nbMonsterBasicToSpawn = nbMonsterBasic;
        int nbItemToSpawn = nbItem;

        int safeIndex = 0;
        while(nbTrapToSpawn > 0 && safeIndex < 1000)
        {
            if(!SpawnObject(sectorController.traps[Random.Range(0, sectorController.traps.Count)]))
                safeIndex++;
            else
                nbTrapToSpawn--;
        }

        safeIndex = 0;
        while(nbObstacleToSpawn > 0 && safeIndex < 1000)
        {
            if(!SpawnObject(sectorController.obstacles[Random.Range(0, sectorController.obstacles.Count)]))
                safeIndex++;
            else
                nbObstacleToSpawn--;
        }

        safeIndex = 0;
        while(nbMonsterBasicToSpawn > 0 && safeIndex < 1000)
        {
            if(!SpawnObject(sectorController.monsters[Random.Range(0, sectorController.monsters.Count)]))
                safeIndex++;
            else
                nbMonsterBasicToSpawn--;
        }

        safeIndex = 0;
        while(nbItemToSpawn > 0 && safeIndex < 1000)
        {
            if(!SpawnItem(sectorController.item[0]))
                safeIndex++;
            else
                nbItemToSpawn--;
        }



        Invoke("launchASTAR", 1);
    }

    private bool SpawnObject(GameObject prefab)
    {
        bool ret = false;
        bool isSpawnable = true;

        int rand_i = Random.Range(0, width);
        int rand_j = Random.Range(0, height);
        
        GameObject go;
        GridBlock block = blockList[rand_i, rand_j];
        GridObject gridObject = prefab.GetComponent<GridObject>();

        // Is full object spawnable ?
        for(int i = rand_i; i < rand_i + gridObject.width; i++)
        {
            for(int j = rand_j; j < rand_j + gridObject.width; j++)
            {
                if(i < width && j < height)
                {
                    GridBlock blockTemp = blockList[i, j];
                    if(!blockTemp.isEmpty)
                        isSpawnable = false;
                }
                else
                    isSpawnable = false;
            }
        }

        // Spawn object
        if(isSpawnable)
        {
            go = Instantiate(prefab);
            go.transform.position = Vector3.zero;
            go.transform.parent = objects.transform;
            go.transform.localPosition = block.getCoordinates() * 0.30f;
            go.transform.localPosition += new Vector3(-13, -3, 0);

            // Fill blocks
            for(int i = rand_i; i < rand_i + gridObject.width; i++)
            {
                for(int j = rand_j; j < rand_j + gridObject.width; j++)
                {
                    GridBlock blockTemp = blockList[i, j];
                    blockTemp.isEmpty = false;
                }
            }

            ret = true;
        }

        return ret;
    }

    private bool SpawnItem(GameObject prefab)
    {
        bool ret = false;
        bool isSpawnable = true;

        int rand_i = Random.Range(0, width);
        int rand_j = Random.Range(0, height);

        GameObject go;
        GridBlock block = blockList[rand_i, rand_j];

        if(!block.isEmpty)
            isSpawnable = false;
        GridObject gridObject = prefab.GetComponent<GridObject>();
        // Spawn object
        if (isSpawnable)
        {
            go = Instantiate(prefab);
            go.transform.position = Vector3.zero;
            go.transform.parent = objects.transform;
            go.transform.localPosition = block.getCoordinates() * 0.30f;
            go.transform.localPosition += new Vector3(-13, -3, 0);

            // Fill blocks
            for(int i = rand_i; i < rand_i + gridObject.width; i++)
            {
                for(int j = rand_j; j < rand_j + gridObject.width; j++)
                {
                    GridBlock blockTemp = blockList[i, j];
                    blockTemp.isEmpty = false;
                }
            }

            ret = true;
        }

        return ret;
    }

    public void ResetSector()
    {
        int nbDechet = objects.transform.GetComponentsInChildren<Dechet>().Length;
        nbTrash += nbDechet;

        foreach(Transform tr in objects.transform)
        {
            Destroy(tr.gameObject);
        }
    }
}
