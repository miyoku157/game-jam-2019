﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class SectorController : MonoBehaviour
{
    [HideInInspector]
    public Sector currentSector;
    [HideInInspector]
    public GameController gameController;

    [Header("Sectors")]
    public Sector[] Sectors;

    [Header("Prefabs")]
    public List<GameObject> traps;
    public List<GameObject> obstacles;
    public List<GameObject> monsters;
    public List<GameObject> monstersElite;
    public List<GameObject> item;
    public List<Sprite> weaponSprites;
    public List<Sprite> armorSprites;
    public List<Sprite> headSprites;
    public AudioSource recyclotron;
    public AudioSource zone1;
    private int currentSectorIndex;

    void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        currentSectorIndex = 0;
    }

    private void Start()
    {
        currentSector = Sectors[0];
    }

    public void GenerateSector()
    {
        currentSector.Generate();
    }

    public void NextSector()
    {
        currentSector.ResetSector();
        currentSector.nbMonsterBasic++;
        currentSectorIndex++;

        if(currentSectorIndex == Sectors.Length)
        {
            currentSectorIndex = 0;
            recyclotron.Play();
            zone1.Stop();
        }

        currentSector = Sectors[currentSectorIndex];

        GenerateSector();
    }
}
