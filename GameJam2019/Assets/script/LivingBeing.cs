﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour
{
    [SerializeField]
    public float range = 2;
    [SerializeField]
    public float speed = 5;
    [SerializeField]
    public float Range = 100;
    [SerializeField]
    public float HP = 100;
    [SerializeField]
    public float HP_max = 100;
    [SerializeField]
    public float armor = 0;
    [SerializeField]
    public float attack = 1;
    [SerializeField]
    public float attackSpeed = 0.2f;
    public Vector3 direction;
    public bool lifeVamp = false;
    public bool MoreDamage = false;
    public List<Item> items;

    void Start()
    {
        
    }

    virtual public void getGameItem(Item recup)
    {
        items.Add(recup);
        recup.gameObject.SetActive(false); 
    }
    
    void Update()
    {

    }
    
}
