﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBlock
{
    int x, y;
    public bool isEmpty;

    public GridBlock(int x0, int y0)
    {
        x = x0;
        y = y0;
        isEmpty = true;
    }

    public void setCoordinates(int x0, int y0)
    {
        x = x0;
        y = y0;
    }

    public Vector2 getCoordinates()
    {
        return new Vector2(x, y);
    }
}
