﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [HideInInspector]
    public SectorController sectorController;
    public AudioSource audio;
    public AudioSource audio2;
    public AudioSource audio3;

    public int waveCounter = 0;
    public int killCounter = 6;
    public int killCounterTemp = 0;

    void Awake()
    {
        sectorController = FindObjectOfType<SectorController>();
    }

    private void Start()
    {
        GameStart();
    }

    public void GameStart()
    {
        sectorController.GenerateSector();
        killCounterTemp = killCounter;
    }

    public void nextWave()
    {
        waveCounter++;
        killCounter++;

        nextSector();
    }

    public void nextSector()
    {
        killCounterTemp = killCounter;
    }

    public void EnnemyKilled()
    {
        killCounterTemp--;

        if(killCounterTemp <= 0)
        {
            sectorController.currentSector.gate.GetComponent<Gate>().Open();
        }
    }

    public void GameOver()
    {
        StartCoroutine(FadeOut(audio2, 0.1f));
        StartCoroutine(FadeOut(audio3, 0.1f));
        StartCoroutine(launchsound(audio));
        //audio.Play();
        /*audio2.Stop();
        audio3.Stop();*/

        Invoke("redirect", 5);
    }
    IEnumerator launchsound(AudioSource a)
    {
        yield return new WaitForSeconds(0.1f);
        a.Play();
    }
    public void redirect()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }
}
