﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Ennemies : LivingBeing
{
    [HideInInspector]
    public GameController gameController;
    [HideInInspector]
    public SectorController sectorController;

    private GameObject[] itemsOnMap;
    private AILerp AIMovement;
    public int sight = 5;
    private AIDestinationSetter AITarget;
    private float compare = 0;
    public GameObject itemPrefab;
    public GameObject[] dechet;
    private bool hasItem = false;
    private SpriteRenderer[] spr;
    private bool flicker = false;

    void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        sectorController = FindObjectOfType<SectorController>();
        int counter = (gameController.waveCounter - 1);

        // Adjust stats with wave counter
        attack = attack * (1f + 0.15f * counter);
        HP_max = HP_max * (1f + 0.15f * counter);
        HP = HP * (1f + 0.15f * counter);

        AIMovement = gameObject.GetComponent<AILerp>();
        AIMovement.speed = speed;
        AITarget = gameObject.GetComponent<AIDestinationSetter>();
        AITarget.player = GameObject.FindWithTag("Player").transform;
        AITarget.range = range;
        AITarget.sight = sight;
        AITarget.target = AITarget.player;
        itemsOnMap = GameObject.FindGameObjectsWithTag("Item");

        foreach(GameObject t in itemsOnMap)
        {
            if(t.activeSelf &&  Vector3.Distance(t.transform.position, transform.position) < sight)
            {
                AITarget.switchTarget(t.transform);
            }
        }
        
        if(Vector3.Distance(transform.position, AITarget.player.transform.position)< Vector3.Distance(transform.position,AITarget.target.transform.position))
        {
            AITarget.switchTarget(AITarget.player);
        }
    }

    private void Start()
    {
        spr = GetComponentsInChildren<SpriteRenderer>();
        StartCoroutine("Flicker"); ;
    }

    void Update()
        {
        itemsOnMap = GameObject.FindGameObjectsWithTag("Item");
            if (AITarget.target == AITarget.player)
            {
                foreach (GameObject t in itemsOnMap)
                {
                
                    if (t.activeSelf && Vector3.Distance(t.transform.position, transform.position) < sight
                        && Vector3.Distance(transform.position, AITarget.player.transform.position) > Vector3.Distance(transform.position, t.transform.position))
                    {
                        AITarget.switchTarget(t.transform);
                    }
                }
            }
            else if(AITarget.target != AITarget.player)
            {
            

            if (Vector3.Distance(transform.position, AITarget.player.transform.position) < Vector3.Distance(transform.position, AITarget.target.transform.position))
                {
                    AITarget.switchTarget(AITarget.player);
                }
                else if (Vector3.Distance(AITarget.target.position, transform.position) < range)
                {
                    this.gameObject.GetComponent<Ennemies>().getGameItem(AITarget.target.gameObject.GetComponent<Item>());
                    AITarget.switchTarget(AITarget.player);
                }
            }
           
        }

    override public void getGameItem(Item recup)
    {
       base.getGameItem(recup);
        if (recup.type == 0)
        {
            attack += recup.PassBonus;
            //attack
        }
        else if (recup.type == 1)
        {
            //defense
            armor += recup.PassBonus;
        }
        else
        {
            //PV
            HP += recup.PassBonus;
        }
        hasItem = true;
    }

    public void takeDamage(float degat)
    {
        GameObject go;
        HP -= degat - (degat * armor / 100);

        if(!flicker)
        {
            flicker = true;
            Invoke("Unflick", 0.3f);
        }

        if(HP <= 0)
        {
            gameController.EnnemyKilled();
            if (hasItem)
                Instantiate(itemPrefab, transform.position, Quaternion.identity);
            else
            {
                int i = Random.Range(0, dechet.Length);
                go = Instantiate(dechet[i], transform.position, Quaternion.identity);
                go.transform.SetParent(sectorController.currentSector.objects.transform);
            }
            Destroy(this.gameObject);

        }
    }

    private void Unflick()
    {
        flicker = false;
        foreach(SpriteRenderer sp in spr)
            sp.color = new Color(255, 255, 255, 255);
    }

    IEnumerator Flicker()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        Color visible = new Color(255, 255, 255, 255);
        Color invisible = new Color(255, 255, 255, 0);
        bool isVisible = true;

        while(true)
        {
            if(flicker)
            {
                if(isVisible)
                {
                    isVisible = false;
                    foreach(SpriteRenderer sp in spr)
                        sp.color = invisible;
                }
                else
                {
                    isVisible = true;
                    foreach(SpriteRenderer sp in spr)
                        sp.color = visible;
                }
            }

            yield return wait;
        }
    }
}


