﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoPlayer : MonoBehaviour
{
    void Start()
    {
        Invoke("GoToMenu", 55);
    }

    void GoToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
