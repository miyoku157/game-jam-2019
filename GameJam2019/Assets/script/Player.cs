﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : LivingBeing
{
    private float timer = 0;
    public int RangeRecupObject = 2;
    public List<Dechet> inventory;
    private int compteur_inventaire = 0;
    public Item arme;
    public Item casque;
    public Item armure;
    private GameObject[] IteminMap ;
    public Image LifeBar;
    public SpriteRenderer spr;
    bool flicker = false;
    bool isAttackingSide = false;
    int max_inventaire = 36;
    public GameObject tuto;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        inventory = new List<Dechet>();
        StartCoroutine("Flicker");
    }
    void setisAttacking()
    {
        if (isAttackingSide)
        {
            isAttackingSide = false;
        }
        else
        {
            isAttackingSide = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"),0);
        direction = (transform.position+ move * speed * Time.deltaTime) - transform.position;
        direction = direction / direction.magnitude;
        var relativePoint0 = transform.InverseTransformPoint(transform.position + move * speed * Time.deltaTime);
        transform.position += move * speed * Time.deltaTime;
        if (!isAttackingSide)
        {

            if (relativePoint0.x < 0.0)
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = false;
            }
            else
            {
                gameObject.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            tuto.SetActive(!tuto.activeSelf);
        }
        if (Input.GetButton("Fire1") && attackSpeed < timer)
        {
            var screenPoint = (Input.mousePosition);
            screenPoint.z = 10.0f; //distance of the plane from the camera
            Vector3 temp =Camera.main.ScreenToWorldPoint(screenPoint);
            var relativePoint = transform.InverseTransformPoint(temp);
            int numColliders = 10;
            Collider2D[] colliders = new Collider2D[numColliders];
            ContactFilter2D contactFilter = new ContactFilter2D();
            // Set you filters here according to https://docs.unity3d.com/ScriptReference/ContactFilter2D.html
            int count = 0;
            Animator anim = gameObject.GetComponent<Animator>();
            if (Mathf.Abs(relativePoint.y) - Mathf.Abs(relativePoint.x) < 0)
            {
                if (relativePoint.x < 0)
                {
                    setisAttacking();
                    Invoke("setisAttacking", 0.5f);
                    gameObject.GetComponent<SpriteRenderer>().flipX = false;
                    anim.SetTrigger("side");

                }
                else
                {
                    
                    anim.SetTrigger("side2");
                }
            }
            else
            {
                if (relativePoint.y < 0)
                {
                    anim.SetTrigger("down");
                }
                else
                {
                    anim.SetTrigger("up");
                }
            }
            if(relativePoint.y < 0.0)
            {
                count = gameObject.transform.GetChild(2).GetComponent<CapsuleCollider2D>().OverlapCollider(contactFilter, colliders);
            }
            else if(relativePoint.y > 0.0)
            {
                count = gameObject.transform.GetChild(3).GetComponent<CapsuleCollider2D>().OverlapCollider(contactFilter, colliders);
            }
            if (relativePoint.x < 0.0)
                count= gameObject.transform.GetChild(0).GetComponent<CapsuleCollider2D>().OverlapCollider(contactFilter, colliders);
            else if (relativePoint.x > 0.0)
                count= gameObject.transform.GetChild(1).GetComponent<CapsuleCollider2D>().OverlapCollider(contactFilter, colliders);
            foreach (Collider2D col in colliders)
            {
                if (col != null)
                {
                    if (col.transform.tag != "Player" && col.transform.tag != "dechet" && col.transform.tag != "Item")
                    {
                        if (col.gameObject.transform.parent.tag == "Enemy")
                        {

                            col.gameObject.transform.parent.gameObject.GetComponent<Ennemies>().takeDamage(attack);
                        }
                    }
                }
            }
            timer = 0;
        }
        if (Input.GetKeyDown("space"))
        {
            Collider2D[] results = new Collider2D[10];
            ContactFilter2D d = new ContactFilter2D();
            Physics2D.OverlapCircle(transform.position, 0.7f, d.NoFilter(), results);
            if(results != null)
            {
                foreach (Collider2D c in results)
                {
                    if (c != null) { 
                        if (c.gameObject.tag == "Item")
                        {
                            getGameItem(c.gameObject.GetComponent<Item>());
                        }
                        else if (c.gameObject.tag == "dechet")
                        {
                            getGameDechet(c.gameObject.GetComponent<Dechet>());
                        }
                        else if (c.gameObject.tag == "recycle")
                        {
                            int count = inventory.Count;
                            count = (int)Mathf.Floor(count / 8);

                            if (c.gameObject.name == "1")
                            {
                                armor += count* (armor * 0.2f);
                            }
                            else if (c.gameObject.name == "2")
                            {
                                HP_max += count *(HP_max * 0.2f);
                            }
                            else
                            {
                                attack += count *(attack * 0.2f);
                            }

                            foreach (Dechet dech in inventory)
                            {
                                if (HP < HP_max)
                                    HP += 2;
                            }
                            inventory.Clear();
                        }
                    }
                }
            }
        }
        if (Input.GetButtonDown("Fire2"))
        {
            if(arme != null)
                arme.itemCapacite(this);
        }

        timer += Time.deltaTime;

        LifeBar.fillAmount = HP / HP_max;
    }
    public void getGameDechet(Dechet recup)
    {
        foreach(Dechet i in inventory)
        {
            if (i != null)
                if (i.equiped)
                    max_inventaire -= 4;
        }
        compteur_inventaire++;
        if(compteur_inventaire < max_inventaire)
            inventory.Add(recup);
        recup.gameObject.SetActive(false);
    }
    public void unequipped(Item recup)
    {
        if (recup.type == 0)
        {
            attack -= recup.PassBonus;
            //attack
        }
        else if (recup.type == 1)
        {
            //defense
            armor -= recup.PassBonus;
        }
        else
        {
            //PV
            HP -= recup.PassBonus;
        }
    }
    public void equipped(Item recup)
    {

        if (recup.type == 0 && arme != null)
        {
            attack += recup.PassBonus;
            arme = recup;
            recup.equiped = true;
            //attack
        }
        else if (recup.type == 1 && armure != null)
        {
            //defense
            armor += recup.PassBonus;
            armure = recup;
            recup.equiped = true;
        }
        else
        {
            if (casque != null)
            {
                //PV
                HP += recup.PassBonus;
                casque = recup;
                recup.equiped = true;
            }
        }
    }
    override public void getGameItem(Item recup)
    {
        
        
        foreach (Dechet i in inventory)
        {
            if (i != null && i is Item)
                if (i.equiped)
                    compteur_inventaire -= 4;
        }
        compteur_inventaire+=4;
        if (compteur_inventaire < max_inventaire)
        {
            if(recup.type == 0)
            {
                if (arme == null)
                    arme = recup;
            }else if (recup.type == 1)
            {
                if (casque == null)
                    casque = recup;
            }
            else
            {
                if (armure == null)
                    armure = recup;
            }
            inventory.Add(recup);
            equipped(recup);
            base.getGameItem(recup);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.transform.parent.tag == "Enemy")
        {
            
            Ennemies e = collision.gameObject.transform.parent.gameObject.GetComponent<Ennemies>();
            takeDamage(e.attack);
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.transform.parent.tag == "Enemy")
        {
            Ennemies e = collision.gameObject.transform.parent.gameObject.GetComponent<Ennemies>();
            takeDamage(e.attack);
        }
    }

    public void takeDamage(float degat)
    {
        if(!flicker)
        {
            HP -= degat - (degat * armor / 100);
            flicker = true;
            Invoke("Unflick", 2);
        }

        if(HP <= 0)
        {
            FindObjectOfType<GameController>().GameOver();
            Destroy(gameObject);
        }
            
    }

    private void Unflick()
    {
        flicker = false;
    }

    IEnumerator Flicker()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);
        Color visible = new Color(255, 255, 255, 255);
        Color invisible = new Color(255, 255, 255, 0);
        bool isVisible = true;

        while(true)
        {
            if(flicker)
            {
                if(isVisible)
                {
                    isVisible = false;
                    spr.color = invisible;
                }
                else
                {
                    isVisible = true;
                    spr.color = visible;
                }
            }
            else
            {
                isVisible = true;
                spr.color = visible;
            }

            yield return wait;
        }
    }
}
