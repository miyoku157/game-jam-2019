﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public float damage = 20;
    public bool activeOnlyOnce = true;
    public Sprite activatedsprite;

    private bool activeOnce;
    private SpriteRenderer spr;
    
    void Start()
    {
        activeOnce = activeOnlyOnce;
        spr = GetComponent<SpriteRenderer>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            spr.sprite = activatedsprite;

            if(activeOnlyOnce && activeOnce)
            {
                col.gameObject.GetComponent<Player>().takeDamage(damage);
                activeOnce = false;
            }
            else if(!activeOnlyOnce)
            {
                col.gameObject.GetComponent<Player>().takeDamage(damage);
            }
        }
    }
}
